#include "gtest/gtest.h"
#include "gmock/gmock.h"
#include "telemetry/formatters.h"

namespace {
using namespace ::testing;

TEST(Formatters, TelemetryValue) {
    telemetry::telemetry_value value = 42;
    ASSERT_THAT(fmt::format("{}", value), Eq("42"));
    ASSERT_THAT(fmt::format("{:+}", value), Eq("+42"));
    ASSERT_THAT(fmt::format("{:4}", value), Eq("  42"));
    ASSERT_THAT(fmt::format("{:04}", value), Eq("0042"));
    ASSERT_THAT(fmt::format("{:^6}", value), Eq("  42  "));
    ASSERT_THAT(fmt::format("{:x}", value), Eq("2a"));
    ASSERT_THAT(fmt::format("{:#04x}", value), Eq("0x2a"));
    ASSERT_THAT(fmt::format("{:#08x}", value), Eq("0x00002a"));
    ASSERT_THAT(fmt::format("{:b}", value), Eq("101010"));
    ASSERT_THAT(fmt::format("{:#b}", value), Eq("0b101010"));

    value = -42;
    ASSERT_THAT(fmt::format("{}", value), Eq("-42"));

    value = 42.0f;
    ASSERT_THAT(fmt::format("{}", value), Eq("42"));
    value = 42.5f;
    ASSERT_THAT(fmt::format("{}", value), Eq("42.500000"));
    value = 42.000003f;
    ASSERT_THAT(fmt::format("{}", value), Eq("42.000003"));

    value = std::monostate{};
    EXPECT_EQ(fmt::format("{}", value), "none");
}

} // namespace