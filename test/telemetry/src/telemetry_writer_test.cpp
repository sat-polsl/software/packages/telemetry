#include "gtest/gtest.h"
#include "gmock/gmock.h"
#include "satos/mock/mutex.h"
#include "telemetry/telemetry_state.h"
#include "telemetry/telemetry_writer.h"

namespace {
using namespace testing;
using namespace telemetry;

struct TelemetryWriterTest : public Test {
    telemetry_value_buffer<16> telemetry_buffer_{};
};

TEST_F(TelemetryWriterTest, WriteSingleValue) {
    telemetry_state telemetry{telemetry_buffer_, {}};

    EXPECT_CALL(*satos::mock::mutex::mutexes[0], lock()).Times(2);
    EXPECT_CALL(*satos::mock::mutex::mutexes[0], unlock()).Times(2);

    std::uint32_t value = 0xdeadc0de;
    telemetry.write(telemetry_id{2}, value);

    std::array<std::byte, 4> buffer;
    telemetry_writer writer(telemetry, buffer);
    auto size = writer.write(telemetry_id{2});

    ASSERT_THAT(size, Eq(4));
    ASSERT_THAT(buffer[0], Eq(std::byte{0xde}));
    ASSERT_THAT(buffer[1], Eq(std::byte{0xc0}));
    ASSERT_THAT(buffer[2], Eq(std::byte{0xad}));
    ASSERT_THAT(buffer[3], Eq(std::byte{0xde}));
}

TEST_F(TelemetryWriterTest, WriteMultipleValues) {
    telemetry_state telemetry{telemetry_buffer_, {}};

    EXPECT_CALL(*satos::mock::mutex::mutexes[0], lock()).Times(6);
    EXPECT_CALL(*satos::mock::mutex::mutexes[0], unlock()).Times(6);

    std::uint32_t value1 = 0xdeadc0de;
    std::uint16_t value2 = 0xbeef;
    std::uint32_t value3 = 0xfee1dead;
    telemetry.write(telemetry_id{1}, value1);
    telemetry.write(telemetry_id{2}, value2);
    telemetry.write(telemetry_id{3}, value3);

    std::array<std::byte, 10> buffer{};
    telemetry_writer writer(telemetry, buffer);
    auto size = writer.write(telemetry_id{3}, telemetry_id{2}, telemetry_id{1});

    ASSERT_THAT(size, Eq(10));
    ASSERT_THAT(buffer[0], Eq(std::byte{0xad}));
    ASSERT_THAT(buffer[1], Eq(std::byte{0xde}));
    ASSERT_THAT(buffer[2], Eq(std::byte{0xe1}));
    ASSERT_THAT(buffer[3], Eq(std::byte{0xfe}));
    ASSERT_THAT(buffer[4], Eq(std::byte{0xef}));
    ASSERT_THAT(buffer[5], Eq(std::byte{0xbe}));
    ASSERT_THAT(buffer[6], Eq(std::byte{0xde}));
    ASSERT_THAT(buffer[7], Eq(std::byte{0xc0}));
    ASSERT_THAT(buffer[8], Eq(std::byte{0xad}));
    ASSERT_THAT(buffer[9], Eq(std::byte{0xde}));
}
} // namespace
