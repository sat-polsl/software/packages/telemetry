#include "gtest/gtest.h"
#include "gmock/gmock.h"
#include "satos/mock/mutex.h"
#include "telemetry/telemetry_serializer.h"
#include "telemetry/telemetry_state.h"

namespace {
using namespace testing;
using namespace telemetry;

struct TelemetryStateTest : public Test {
    telemetry_value_buffer<16> telemetry_buffer_{};
    telemetry_observer_buffer<4> telemetry_observer_buffer_{};
};

TEST_F(TelemetryStateTest, WriteRead) {
    telemetry_state telemetry{telemetry_buffer_, {}};

    std::uint32_t value = 0xdeadc0de;

    EXPECT_CALL(*satos::mock::mutex::mutexes[0], lock()).Times(4);
    EXPECT_CALL(*satos::mock::mutex::mutexes[0], unlock()).Times(4);

    telemetry.write(telemetry_id{2}, value);

    auto template_result = telemetry.read<std::uint32_t>(telemetry_id{2});

    ASSERT_THAT(template_result.has_value(), Eq(true));
    ASSERT_THAT(*template_result, Eq(0xdeadc0de));

    auto result = telemetry.read(telemetry_id{2});
    ASSERT_THAT(std::holds_alternative<std::uint32_t>(result), Eq(true));
    ASSERT_THAT(std::get<std::uint32_t>(result), Eq(0xdeadc0de));

    auto optional_result = telemetry.read<float>(telemetry_id{2});
    ASSERT_THAT(optional_result.has_value(), Eq(false));
}

TEST_F(TelemetryStateTest, SerializeU8) {
    telemetry_state telemetry{telemetry_buffer_, {}};
    auto serializer = telemetry_serializer{telemetry};

    std::uint8_t value = 0xaa;

    EXPECT_CALL(*satos::mock::mutex::mutexes[0], lock()).Times(2);
    EXPECT_CALL(*satos::mock::mutex::mutexes[0], unlock()).Times(2);

    telemetry.write(telemetry_id{2}, value);

    std::array<std::byte, 1> buffer{};

    auto result = serializer.serialize(telemetry_id{2}, buffer);

    ASSERT_THAT(result, Eq(1));
    ASSERT_THAT(buffer[0], Eq(std::byte{0xaa}));
}

TEST_F(TelemetryStateTest, SerializeU16) {
    telemetry_state telemetry{telemetry_buffer_, {}};
    auto serializer = telemetry_serializer{telemetry};

    std::uint16_t value = 0xdead;

    EXPECT_CALL(*satos::mock::mutex::mutexes[0], lock()).Times(2);
    EXPECT_CALL(*satos::mock::mutex::mutexes[0], unlock()).Times(2);

    telemetry.write(telemetry_id{2}, value);

    std::array<std::byte, 2> buffer{};

    auto result = serializer.serialize(telemetry_id{2}, buffer);

    ASSERT_THAT(result, Eq(2));
    ASSERT_THAT(buffer[0], Eq(std::byte{0xad}));
    ASSERT_THAT(buffer[1], Eq(std::byte{0xde}));
}

TEST_F(TelemetryStateTest, SerializeU32) {
    telemetry_state telemetry{telemetry_buffer_, {}};
    auto serializer = telemetry_serializer{telemetry};

    std::uint32_t value = 0xdeadc0de;

    EXPECT_CALL(*satos::mock::mutex::mutexes[0], lock()).Times(2);
    EXPECT_CALL(*satos::mock::mutex::mutexes[0], unlock()).Times(2);

    telemetry.write(telemetry_id{2}, value);

    std::array<std::byte, 4> buffer{};

    auto result = serializer.serialize(telemetry_id{2}, buffer);

    ASSERT_THAT(result, Eq(4));
    ASSERT_THAT(buffer[0], Eq(std::byte{0xde}));
    ASSERT_THAT(buffer[1], Eq(std::byte{0xc0}));
    ASSERT_THAT(buffer[2], Eq(std::byte{0xad}));
    ASSERT_THAT(buffer[3], Eq(std::byte{0xde}));
}

TEST_F(TelemetryStateTest, SerializeI8) {
    telemetry_state telemetry{telemetry_buffer_, {}};
    auto serializer = telemetry_serializer{telemetry};

    std::int8_t value = -1;

    EXPECT_CALL(*satos::mock::mutex::mutexes[0], lock()).Times(2);
    EXPECT_CALL(*satos::mock::mutex::mutexes[0], unlock()).Times(2);

    telemetry.write(telemetry_id{2}, value);

    std::array<std::byte, 1> buffer{};

    auto result = serializer.serialize(telemetry_id{2}, buffer);

    ASSERT_THAT(result, Eq(1));
    ASSERT_THAT(buffer[0], Eq(std::byte{0xff}));
}

TEST_F(TelemetryStateTest, SerializeI16) {
    telemetry_state telemetry{telemetry_buffer_, {}};
    auto serializer = telemetry_serializer{telemetry};

    std::int16_t value = -12345;

    EXPECT_CALL(*satos::mock::mutex::mutexes[0], lock()).Times(2);
    EXPECT_CALL(*satos::mock::mutex::mutexes[0], unlock()).Times(2);

    telemetry.write(telemetry_id{2}, value);

    std::array<std::byte, 2> buffer{};

    auto result = serializer.serialize(telemetry_id{2}, buffer);

    ASSERT_THAT(result, Eq(2));
    ASSERT_THAT(buffer[0], Eq(std::byte{0xc7}));
    ASSERT_THAT(buffer[1], Eq(std::byte{0xcf}));
}

TEST_F(TelemetryStateTest, SerializeI32) {
    telemetry_state telemetry{telemetry_buffer_, {}};
    auto serializer = telemetry_serializer{telemetry};

    std::int32_t value = -1234567;

    EXPECT_CALL(*satos::mock::mutex::mutexes[0], lock()).Times(2);
    EXPECT_CALL(*satos::mock::mutex::mutexes[0], unlock()).Times(2);

    telemetry.write(telemetry_id{2}, value);

    std::array<std::byte, 4> buffer{};

    auto result = serializer.serialize(telemetry_id{2}, buffer);

    ASSERT_THAT(result, Eq(4));
    ASSERT_THAT(buffer[0], Eq(std::byte{0x79}));
    ASSERT_THAT(buffer[1], Eq(std::byte{0x29}));
    ASSERT_THAT(buffer[2], Eq(std::byte{0xed}));
    ASSERT_THAT(buffer[3], Eq(std::byte{0xff}));
}

TEST_F(TelemetryStateTest, SerializeBool) {
    telemetry_state telemetry{telemetry_buffer_, {}};
    auto serializer = telemetry_serializer{telemetry};

    bool value = true;

    EXPECT_CALL(*satos::mock::mutex::mutexes[0], lock()).Times(2);
    EXPECT_CALL(*satos::mock::mutex::mutexes[0], unlock()).Times(2);

    telemetry.write(telemetry_id{2}, value);

    std::array<std::byte, 1> buffer{};

    auto result = serializer.serialize(telemetry_id{2}, buffer);

    ASSERT_THAT(result, Eq(1));
    ASSERT_THAT(buffer[0], Eq(std::byte{0x01}));
}

TEST_F(TelemetryStateTest, SerializeFloat) {
    telemetry_state telemetry{telemetry_buffer_, {}};
    auto serializer = telemetry_serializer{telemetry};

    float value = 1.23456f;

    EXPECT_CALL(*satos::mock::mutex::mutexes[0], lock()).Times(2);
    EXPECT_CALL(*satos::mock::mutex::mutexes[0], unlock()).Times(2);

    telemetry.write(telemetry_id{2}, value);

    std::array<std::byte, 4> buffer{};

    auto result = serializer.serialize(telemetry_id{2}, buffer);

    ASSERT_THAT(result, Eq(4));
    ASSERT_THAT(buffer[0], Eq(std::byte{0x10}));
    ASSERT_THAT(buffer[1], Eq(std::byte{0x06}));
    ASSERT_THAT(buffer[2], Eq(std::byte{0x9e}));
    ASSERT_THAT(buffer[3], Eq(std::byte{0x3f}));
}

TEST_F(TelemetryStateTest, SerializeChar) {
    telemetry_state telemetry{telemetry_buffer_, {}};
    auto serializer = telemetry_serializer{telemetry};

    char value = 'a';

    EXPECT_CALL(*satos::mock::mutex::mutexes[0], lock()).Times(2);
    EXPECT_CALL(*satos::mock::mutex::mutexes[0], unlock()).Times(2);

    telemetry.write(telemetry_id{2}, value);

    std::array<std::byte, 1> buffer{};

    auto result = serializer.serialize(telemetry_id{2}, buffer);

    ASSERT_THAT(result, Eq(1));
    ASSERT_THAT(buffer[0], Eq(std::byte{0x61}));
}

TEST_F(TelemetryStateTest, SerializeByte) {
    telemetry_state telemetry{telemetry_buffer_, {}};
    auto serializer = telemetry_serializer{telemetry};

    auto value = std::byte{0xaa};

    EXPECT_CALL(*satos::mock::mutex::mutexes[0], lock()).Times(2);
    EXPECT_CALL(*satos::mock::mutex::mutexes[0], unlock()).Times(2);

    telemetry.write(telemetry_id{2}, value);

    std::array<std::byte, 1> buffer{};

    auto result = serializer.serialize(telemetry_id{2}, buffer);

    ASSERT_THAT(result, Eq(1));
    ASSERT_THAT(buffer[0], Eq(std::byte{0xaa}));
}

TEST_F(TelemetryStateTest, SerializeEmpty) {
    telemetry_state telemetry{telemetry_buffer_, {}};
    auto serializer = telemetry_serializer{telemetry};

    EXPECT_CALL(*satos::mock::mutex::mutexes[0], lock()).Times(1);
    EXPECT_CALL(*satos::mock::mutex::mutexes[0], unlock()).Times(1);

    std::array<std::byte, 1> buffer{};

    auto result = serializer.serialize(telemetry_id{2}, buffer);

    ASSERT_THAT(result, Eq(0));
}

TEST_F(TelemetryStateTest, ObserverCalled) {
    telemetry_state telemetry{telemetry_buffer_, telemetry_observer_buffer_};

    bool observer_called = false;

    telemetry_observer observer{[&observer_called](telemetry_id id, telemetry_state& state) {
        EXPECT_THAT(id, Eq(telemetry_id{2}));
        auto value = state.read<std::uint32_t>(id);
        EXPECT_THAT(value.has_value(), true);
        EXPECT_THAT(value.value(), Eq(std::uint32_t{12345}));

        observer_called = true;
    }};

    telemetry.attach(observer);

    telemetry.write(telemetry_id{2}, std::uint32_t{12345});

    ASSERT_THAT(observer_called, Eq(true));
}

TEST_F(TelemetryStateTest, MultipleObserversCalled) {
    telemetry_state telemetry{telemetry_buffer_, telemetry_observer_buffer_};

    bool observer1_called = false;
    bool observer2_called = false;

    telemetry_observer observer1{[&observer1_called](telemetry_id id, telemetry_state& state) {
        EXPECT_THAT(id, Eq(telemetry_id{2}));
        auto value = state.read<std::uint32_t>(id);
        EXPECT_THAT(value.has_value(), true);
        EXPECT_THAT(value.value(), Eq(std::uint32_t{12345}));

        observer1_called = true;
    }};

    telemetry_observer observer2{[&observer2_called](telemetry_id id, telemetry_state& state) {
        EXPECT_THAT(id, Eq(telemetry_id{2}));
        auto value = state.read<std::uint32_t>(id);
        EXPECT_THAT(value.has_value(), true);
        EXPECT_THAT(value.value(), Eq(std::uint32_t{12345}));

        observer2_called = true;
    }};

    telemetry.attach(observer1);
    telemetry.attach(observer2);

    telemetry.write(telemetry_id{2}, std::uint32_t{12345});

    ASSERT_THAT(observer1_called, Eq(true));
    ASSERT_THAT(observer2_called, Eq(true));
}

TEST_F(TelemetryStateTest, DetachObserver) {
    telemetry_state telemetry{telemetry_buffer_, telemetry_observer_buffer_};

    std::uint32_t call_counter = 0;

    telemetry_observer observer{[&call_counter](telemetry_id id, telemetry_state& state) {
        EXPECT_THAT(id, Eq(telemetry_id{2}));
        auto value = state.read<std::uint32_t>(id);
        EXPECT_THAT(value.has_value(), true);
        EXPECT_THAT(value.value(), Eq(std::uint32_t{12345}));

        call_counter++;
    }};

    telemetry.attach(observer);

    telemetry.write(telemetry_id{2}, std::uint32_t{12345});

    telemetry.detach(observer);

    telemetry.write(telemetry_id{2}, std::uint32_t{12345});

    ASSERT_THAT(call_counter, Eq(1));
}

} // namespace
