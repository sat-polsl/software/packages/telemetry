#include "gtest/gtest.h"
#include "gmock/gmock.h"
#include "telemetry/telemetry_decoder.h"
#include "telemetry/telemetry_encoder.h"
#include "telemetry/telemetry_state.h"

namespace {
using namespace testing;
using namespace telemetry;

struct TelemetryDecoderTest : public Test {
    telemetry_value_buffer<16> telemetry_buffer_{};
};

template<typename T>
void assert_decoded_value(telemetry_decoder& decoder, std::uint32_t expected_id, auto matcher) {
    auto result = decoder.decode();
    ASSERT_THAT(result.has_value(), Eq(true));
    ASSERT_THAT(result.value().id, Eq(expected_id));
    auto value = result.value().value;
    auto* p = std::get_if<T>(&value);
    ASSERT_THAT(p, Ne(nullptr));
    ASSERT_THAT(*p, matcher);
}

TEST_F(TelemetryDecoderTest, EncodeDecode) {
    telemetry_state telemetry{telemetry_buffer_, {}};
    std::uint32_t value1 = 0xdeadc0de;
    std::uint16_t value2 = 0xbeef;
    std::int8_t value3 = -100;
    char value4 = 'A';
    float value5 = 21.37f;
    telemetry.write(telemetry_id{1}, value1);
    telemetry.write(telemetry_id{2}, value2);
    telemetry.write(telemetry_id{3}, value3);
    telemetry.write(telemetry_id{4}, value4);
    telemetry.write(telemetry_id{5}, value5);

    std::array<std::byte, 22> buffer{};
    telemetry_encoder encoder(telemetry, buffer);

    std::size_t size{};
    size += encoder.encode(telemetry_id{1});
    size += encoder.encode(telemetry_id{2});
    size += encoder.encode(telemetry_id{3});
    size += encoder.encode(telemetry_id{4});
    size += encoder.encode(telemetry_id{5});
    size += encoder.encode(telemetry_id{6});

    ASSERT_THAT(size, Eq(22));

    telemetry_decoder decoder(buffer);

    assert_decoded_value<std::uint32_t>(decoder, 1, Eq(0xdeadc0de));
    assert_decoded_value<std::uint16_t>(decoder, 2, Eq(0xbeef));
    assert_decoded_value<std::int8_t>(decoder, 3, Eq(-100));
    assert_decoded_value<char>(decoder, 4, Eq('A'));
    assert_decoded_value<float>(decoder, 5, FloatEq(21.37f));
}
} // namespace
