#include "gtest/gtest.h"
#include "gmock/gmock.h"
#include "telemetry/telemetry_encoder.h"
#include "telemetry/telemetry_state.h"

namespace {
using namespace testing;
using namespace telemetry;

struct TelemetryEncoderTest : public Test {
    telemetry_value_buffer<16> telemetry_buffer_{};
};

TEST_F(TelemetryEncoderTest, EncodeMultipleValues) {
    telemetry_state telemetry{telemetry_buffer_, {}};
    std::uint32_t value1 = 0xdeadc0de;
    std::uint16_t value2 = 0xbeef;
    std::int8_t value3 = 0xaa;
    telemetry.write(telemetry_id{1}, value1);
    telemetry.write(telemetry_id{2}, value2);
    telemetry.write(telemetry_id{3}, value3);

    std::array<std::byte, 16> buffer{};

    telemetry_encoder encoder(telemetry, buffer);

    std::size_t size{};
    size += encoder.encode(telemetry_id{1});
    size += encoder.encode(telemetry_id{2});
    size += encoder.encode(telemetry_id{3});
    size += encoder.encode(telemetry_id{4});

    ASSERT_THAT(size, Eq(13));
    ASSERT_THAT(buffer[0], Eq(std::byte{0x13}));
    ASSERT_THAT(buffer[1], Eq(std::byte{0x00}));
    ASSERT_THAT(buffer[2], Eq(std::byte{0xde}));
    ASSERT_THAT(buffer[3], Eq(std::byte{0xc0}));
    ASSERT_THAT(buffer[4], Eq(std::byte{0xad}));
    ASSERT_THAT(buffer[5], Eq(std::byte{0xde}));

    ASSERT_THAT(buffer[6], Eq(std::byte{0x22}));
    ASSERT_THAT(buffer[7], Eq(std::byte{0x00}));
    ASSERT_THAT(buffer[8], Eq(std::byte{0xef}));
    ASSERT_THAT(buffer[9], Eq(std::byte{0xbe}));

    ASSERT_THAT(buffer[10], Eq(std::byte{0x34}));
    ASSERT_THAT(buffer[11], Eq(std::byte{0x00}));
    ASSERT_THAT(buffer[12], Eq(std::byte{0xaa}));

    ASSERT_THAT(buffer[13], Eq(std::byte{0x00}));
    ASSERT_THAT(buffer[14], Eq(std::byte{0x00}));
    ASSERT_THAT(buffer[15], Eq(std::byte{0x00}));
}

TEST_F(TelemetryEncoderTest, EncodeBufferTooShort) {
    telemetry_state telemetry{telemetry_buffer_, {}};
    std::uint32_t value1 = 0xdeadc0de;
    telemetry.write(telemetry_id{1}, value1);

    std::array<std::byte, 5> buffer{};

    telemetry_encoder encoder(telemetry, buffer);

    std::size_t size{};
    size += encoder.encode(telemetry_id{1});

    ASSERT_THAT(size, Eq(0));
    ASSERT_THAT(buffer[0], Eq(std::byte{}));
    ASSERT_THAT(buffer[1], Eq(std::byte{}));
    ASSERT_THAT(buffer[2], Eq(std::byte{}));
    ASSERT_THAT(buffer[3], Eq(std::byte{}));
    ASSERT_THAT(buffer[4], Eq(std::byte{}));
}

} // namespace
