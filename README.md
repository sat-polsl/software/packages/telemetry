[![Quality Gate Status](https://sonarcloud.io/api/project_badges/measure?project=sat-polsl_telemetry&metric=alert_status)](https://sonarcloud.io/summary/new_code?id=sat-polsl_telemetry)
[![Coverage](https://sonarcloud.io/api/project_badges/measure?project=sat-polsl_telemetry&metric=coverage)](https://sonarcloud.io/summary/new_code?id=sat-polsl_telemetry)
[![Pipeline](https://gitlab.com/sat-polsl/software/packages/telemetry/badges/main/pipeline.svg)](https://gitlab.com/sat-polsl/software/packages/telemetry/-/pipelines?page=1&scope=all&ref=main)
[![Version](https://img.shields.io/badge/dynamic/json?color=blue&label=Version&query=%24%5B%3A1%5D.name&url=https%3A%2F%2Fgitlab.com%2Fapi%2Fv4%2Fprojects%2F42795933%2Frepository%2Ftags)](https://gitlab.com/sat-polsl/software/packages/telemetry/-/tags)
# Telemetry

## Overview

This package provides:
- telemetry state that can store defined number of fundamental type values.
- telemetry serializer that can serialize data stored in telemetry state
- telemetry writer that can write multiple serialized data stored in telemetry state to bytes buffer.
- telemetry service that can execute samplers with proper timing

### Rules

1. Use conventional commits.
2. Update tag with version after merging to `main`.
3. Use `snake_case` for everything except template arguments which are named with `CamelCase`

## Usage:

Create state that can store 10 different values and write/read some values:
```c++
telemetry::telemetry_state<10> state;

std::uint32_t u1 = 0xdeadc0de;
float f1 = 1.23456;

state.write(0, u32);
state.write(1, f);

auto u2 = state.read<std::uint32_t>(0); // contains std::optional with std::uint32_t equal to u1
auto f2 = state.read<float>(1); // contains std::optional with float equal to f
```

Serialize data to byte buffer
```c++
telemetry::telemetry_state<10> state;

std::uint32_t u1 = 0xdeadc0de;
float f1 = 1.23456;

state.write(0, u32);
state.write(1, f);

std::array<std::byte, 8> buffer;

telemetry::telemetry_writer writer(state, buffer);
writer.write(0, 1);
```








