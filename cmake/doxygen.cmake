set(DOXYGEN_SOURCES
    ${CMAKE_SOURCE_DIR}/telemetry
    ${CMAKE_SOURCE_DIR}/README.md
    )

set(DOXYGEN_FILE_PATTERNS
    *.h
    *.cpp
    )

set(DOXYGEN_OUTPUT_DIRECTORY "${CMAKE_BINARY_DIR}/docs")
set(DOXYGEN_USE_MDFILE_AS_MAINPAGE ${CMAKE_SOURCE_DIR}/README.md)
set(DOXYGEN_PROJECT_NAME "Telemetry")

doxygen_add_docs(docs ${DOXYGEN_SOURCES} WORKING_DIRECTORY ${CMAKE_SOURCE_DIR})
