function(add_unit_test TARGET)
    list(APPEND UNIT_TESTS_LIST ${TARGET})
    set(UNIT_TESTS_LIST ${UNIT_TESTS_LIST} CACHE INTERNAL "")

    target_link_libraries(${TARGET} PRIVATE
        gmock_main
        semihosting
        )

    set_property(TARGET ${TARGET}
        PROPERTY
        CROSSCOMPILING_EMULATOR ${QEMU_RUNNER} ${QEMU_EXECUTABLE} ${TARGET_CORE}
        )

    gtest_discover_tests(${TARGET})
endfunction()
