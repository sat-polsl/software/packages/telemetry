#pragma once
#include "fmt/core.h"
#include "fmt/format.h"
#include "satext/overload.h"
#include "satext/type_traits.h"
#include "telemetry/types.h"

template<>
class fmt::formatter<telemetry::telemetry_value> {
public:
    constexpr auto parse(fmt::format_parse_context& ctx) -> decltype(ctx.begin()) {
        auto begin = ctx.begin();
        auto end = ctx.end();
        if (begin == end)
            return begin;
        auto type = detail::type_constant<int, char>::value;
        return detail::parse_format_specs(begin, end, specs_, ctx, type);
    }

    template<typename FormatContext>
    auto format(const telemetry::telemetry_value& value, FormatContext& ctx)
        -> decltype(ctx.out()) {
        return std::visit(
            satext::overload(
                [&ctx](std::monostate) { return fmt::format_to(ctx.out(), "none"); },
                [&ctx, this](std::byte b) { return format(satext::to_underlying_type(b), ctx); },
                [&ctx, this](auto v) { return format(v, ctx); }),
            value);
    }

    auto format(const auto& val, auto& ctx) -> decltype(ctx.out()) {
        auto specs = specs_;
        if (specs_.width_ref.kind != detail::arg_id_kind::none ||
            specs_.precision_ref.kind != detail::arg_id_kind::none) {
            detail::handle_dynamic_spec<detail::width_checker>(specs.width, specs.width_ref, ctx);
            detail::handle_dynamic_spec<detail::precision_checker>(
                specs.precision, specs.precision_ref, ctx);
        }
        return detail::write<char>(ctx.out(), val, specs, ctx.locale());
    }

    auto format(const float& f, auto& ctx) -> decltype(ctx.out()) {
        static constexpr auto precision = 6;
        float integral;
        auto fractional = static_cast<int>(std::modf(f, &integral) * std::pow(10, precision));

        if (fractional == 0) {
            return fmt::format_to(ctx.out(), "{}", static_cast<int>(integral));
        } else {
            return fmt::format_to(ctx.out(), "{}.{:06}", static_cast<int>(integral), fractional);
        }
    }

private:
    detail::dynamic_format_specs<char> specs_;
};
