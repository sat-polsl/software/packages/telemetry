#pragma once
#include <cstdint>
#include <optional>
#include "gmock/gmock.h"
#include "telemetry/enums.h"
#include "telemetry/telemetry_state_concept.h"
#include "telemetry/types.h"

namespace telemetry::mock {
struct telemetry_state {
    MOCK_METHOD(telemetry_value, read, (telemetry_id));
    MOCK_METHOD(telemetry_value, templated_read, (telemetry_id));

    template<typename T>
    std::optional<T> read(telemetry_id id) {
        auto value = templated_read(id);
        if (auto* p = std::get_if<T>(&value)) {
            return *p;
        } else {
            return std::nullopt;
        }
    }

    MOCK_METHOD(void, write_u8, (telemetry_id, std::uint8_t));
    MOCK_METHOD(void, write_u16, (telemetry_id, std::uint16_t));
    MOCK_METHOD(void, write_u32, (telemetry_id, std::uint32_t));
    MOCK_METHOD(void, write_i8, (telemetry_id, std::int8_t));
    MOCK_METHOD(void, write_i16, (telemetry_id, std::int16_t));
    MOCK_METHOD(void, write_i32, (telemetry_id, std::int32_t));
    MOCK_METHOD(void, write_bool, (telemetry_id, bool));
    MOCK_METHOD(void, write_float, (telemetry_id, float));
    MOCK_METHOD(void, write_char, (telemetry_id, char));
    MOCK_METHOD(void, write_byte, (telemetry_id, std::byte));
    MOCK_METHOD(void, write_monostate, (telemetry_id, std::monostate));
    MOCK_METHOD(void, write_telemetry_value, (telemetry_id, telemetry_value));

    void write(telemetry_id id, std::uint8_t value) { write_u8(id, value); }
    void write(telemetry_id id, std::uint16_t value) { write_u16(id, value); }
    void write(telemetry_id id, std::uint32_t value) { write_u32(id, value); }
    void write(telemetry_id id, std::int8_t value) { write_i8(id, value); }
    void write(telemetry_id id, std::int16_t value) { write_i16(id, value); }
    void write(telemetry_id id, std::int32_t value) { write_i32(id, value); }
    void write(telemetry_id id, bool value) { write_bool(id, value); }
    void write(telemetry_id id, float value) { write_float(id, value); }
    void write(telemetry_id id, char value) { write_char(id, value); }
    void write(telemetry_id id, std::byte value) { write_byte(id, value); }
    void write(telemetry_id id, std::monostate value) { write_monostate(id, value); }
    void write(telemetry_id id, telemetry_value value) { write_telemetry_value(id, value); }
};

static_assert(telemetry_state_writable<telemetry_state>);
} // namespace telemetry::mock
