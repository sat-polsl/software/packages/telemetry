#include "telemetry/telemetry_decoder.h"
#include "satext/struct.h"
#include "telemetry/enums.h"

using namespace satext::struct_literals;

namespace telemetry {
telemetry_decoder::telemetry_decoder::telemetry_decoder(std::span<const std::byte> buffer) :
    buffer_{buffer} {}

template<typename T>
std::tuple<telemetry_value, std::span<const std::byte>>
get_value(std::span<const std::byte> buffer) {
    satext::struct_detail::data_view view{buffer.data(), false};
    if (buffer.size() < sizeof(T)) {
        return {std::monostate{}, buffer};
    }
    return {satext::struct_detail::data::get<T>(view), buffer.subspan(sizeof(T))};
}

std::tuple<telemetry_value, std::span<const std::byte>>
get_value(type_id type, std::span<const std::byte> buffer) {
    switch (type) {
    case type_id::none:
        return {std::monostate{}, buffer};
    case type_id::u8:
        return get_value<std::uint8_t>(buffer);
    case type_id::u16:
        return get_value<std::uint16_t>(buffer);
    case type_id::u32:
        return get_value<std::uint32_t>(buffer);
    case type_id::i8:
        return get_value<std::int8_t>(buffer);
    case type_id::i16:
        return get_value<std::int16_t>(buffer);
    case type_id::i32:
        return get_value<std::int32_t>(buffer);
    case type_id::boolean:
        return get_value<bool>(buffer);
    case type_id::f32:
        return get_value<float>(buffer);
    case type_id::character: {
        satext::struct_detail::data_view view{std::as_bytes(buffer).data(), false};
        if (buffer.size() < sizeof(std::uint8_t)) {
            return {std::monostate{}, buffer};
        }
        return {static_cast<char>(satext::struct_detail::data::get<std::uint8_t>(view)),
                buffer.subspan(sizeof(char))};
    }
    case type_id::byte: {
        satext::struct_detail::data_view view{std::as_bytes(buffer).data(), false};
        if (buffer.size() < sizeof(std::uint8_t)) {
            return {std::monostate{}, buffer};
        }
        return {static_cast<std::byte>(satext::struct_detail::data::get<std::uint8_t>(view)),
                buffer.subspan(sizeof(std::byte))};
    }
    }
    return {std::monostate{}, buffer};
}

satext::expected<key_value, std::monostate> telemetry_decoder::decode() {
    return satext::unpack("<H"_fmt, buffer_)
        .and_then([this](auto unpacked) -> satext::expected<key_value, std::monostate> {
            buffer_ = buffer_.subspan(key_size);
            auto [key] = unpacked;
            std::uint16_t index = key >> index_shift;
            auto value_type = type_id(key & type_mask);

            auto [value, buffer] = get_value(value_type, buffer_);
            buffer_ = buffer;

            return key_value{index, value};
        });
}

bool telemetry_decoder::is_empty() const { return buffer_.empty(); }

} // namespace telemetry
