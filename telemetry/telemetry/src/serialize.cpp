#include "telemetry/serialize.h"
#include "satext/overload.h"
#include "satext/struct.h"

namespace telemetry {

using namespace satext::struct_literals;

std::size_t serialize(telemetry_value value, std::span<std::byte> buffer) {
    return std::visit(
        satext::overload([](std::monostate) { return std::size_t{}; },
                         [buffer](std::uint8_t x) { return satext::pack_to("<B"_fmt, buffer, x); },
                         [buffer](std::uint16_t x) { return satext::pack_to("<H"_fmt, buffer, x); },
                         [buffer](std::uint32_t x) { return satext::pack_to("<I"_fmt, buffer, x); },
                         [buffer](std::int8_t x) { return satext::pack_to("<b"_fmt, buffer, x); },
                         [buffer](std::int16_t x) { return satext::pack_to("<h"_fmt, buffer, x); },
                         [buffer](std::int32_t x) { return satext::pack_to("<i"_fmt, buffer, x); },
                         [buffer](bool x) {
                             return satext::pack_to("<B"_fmt, buffer, static_cast<std::uint8_t>(x));
                         },
                         [buffer](float x) { return satext::pack_to("<f"_fmt, buffer, x); },
                         [buffer](char x) { return satext::pack_to("<c"_fmt, buffer, x); },
                         [buffer](std::byte x) {
                             return satext::pack_to("<B"_fmt, buffer, static_cast<std::uint8_t>(x));
                         }),
        value);
}

} // namespace telemetry
