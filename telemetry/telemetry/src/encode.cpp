#include "telemetry/encode.h"
#include "satext/struct.h"
#include "satext/type_traits.h"
#include "telemetry/get_type_id.h"
#include "telemetry/serialize.h"

namespace telemetry {

using namespace satext::struct_literals;

constexpr auto index_shift = 4u;
constexpr auto key_size = sizeof(std::uint16_t);

std::size_t
encode(telemetry::telemetry_id id, telemetry::telemetry_value value, std::span<std::byte> buffer) {
    if (buffer.size() < key_size) {
        return 0;
    }

    auto value_size = serialize(value, buffer.subspan(key_size));
    if (value_size == 0) {
        return 0;
    }

    std::uint16_t key = satext::to_underlying_type(id) << index_shift |
                        satext::to_underlying_type(get_type_id(value));
    auto written_key_size = satext::pack_to("<H"_fmt, buffer, key);
    return written_key_size + value_size;
}
} // namespace telemetry
