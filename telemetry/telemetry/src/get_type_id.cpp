#include "telemetry/get_type_id.h"

namespace telemetry {
type_id get_type_id(telemetry_value value) {
    return std::visit(satext::overload([](std::monostate) { return type_id::none; },
                                       [](std::uint8_t) { return type_id::u8; },
                                       [](std::uint16_t) { return type_id::u16; },
                                       [](std::uint32_t) { return type_id::u32; },
                                       [](std::int8_t) { return type_id::i8; },
                                       [](std::int16_t) { return type_id::i16; },
                                       [](std::int32_t) { return type_id::i32; },
                                       [](bool) { return type_id::boolean; },
                                       [](float) { return type_id::f32; },
                                       [](char) { return type_id::character; },
                                       [](std::byte) { return type_id::byte; }),
                      value);
}
} // namespace telemetry
