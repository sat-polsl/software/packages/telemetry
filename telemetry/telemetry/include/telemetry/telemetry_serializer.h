#pragma once
#include <variant>
#include "satext/overload.h"
#include "satext/struct.h"
#include "telemetry/serialize.h"
#include "telemetry/telemetry_state_concept.h"

namespace telemetry {

/**
 * @ingroup telemetry
 * @{
 */

/**
 * @brief Class providing binary serialization of values in Telemetry State to byte buffer.
 * @tparam State Telemetry State type.
 */
template<telemetry::telemetry_state_concept State>
class telemetry_serializer {
public:
    /**
     * @brief Constructor.
     * @param state Telemetry State reference.
     */
    explicit telemetry_serializer(State& state) : state_{state} {}
    telemetry_serializer(const telemetry_serializer&) = default;
    telemetry_serializer& operator=(const telemetry_serializer&) = default;
    telemetry_serializer(telemetry_serializer&&) noexcept = default;
    telemetry_serializer& operator=(telemetry_serializer&&) noexcept = default;

    /**
     * @brief Serializes value to given buffer.
     * @param id Telemetry ID.
     * @param buffer buffer.
     * @return Size of written value.
     */
    std::size_t serialize(telemetry::telemetry_id id, std::span<std::byte> buffer) {
        return ::telemetry::serialize(state_.read(id), buffer);
    }

private:
    State& state_;
};

/** @} */

} // namespace telemetry
