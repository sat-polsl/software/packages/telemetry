#pragma once
#include <concepts>
#include <cstdint>
#include <mutex>
#include <ranges>
#include <span>
#include <variant>
#include "etl/vector.h"
#include "satext/noncopyable.h"
#include "satext/overload.h"
#include "satext/struct.h"
#include "satext/type_traits.h"
#include "satext/expected.h"
#include "satos/mutex.h"
#include "telemetry/telemetry_observer.h"
#include "telemetry/telemetry_state_concept.h"
#include "telemetry/types.h"

namespace telemetry {
/**
 * @ingroup telemetry
 * @{
 */

/**
 * @brief Telemetry state holding N values of fundamental types (up to 32 bits types).
 */
class telemetry_state : private satext::noncopyable {
public:
    explicit telemetry_state(std::span<telemetry_value> state_buffer,
                             std::span<telemetry_observer*> observers_buffer) :
        state_{state_buffer},
        observers_{observers_buffer.data(), observers_buffer.size()} {}

    /**
     * @brief Writes value at given ID.
     * @tparam T Value type.
     * @param id Telemetry ID.
     * @param value Value.
     */
    template<typename T>
    void write(telemetry_id id, T value) {
        std::lock_guard lck(mtx_);
        state_[satext::to_underlying_type(id)] = value;
        for (const auto* observer : observers_) {
            observer->update(id, *this);
        }
    }

    /**
     * @brief Reads value from given ID.
     * @param index Telemetry ID.
     * @return Variant holding value at given ID.
     */
    auto read(telemetry_id id) {
        std::lock_guard lck(mtx_);
        return state_[satext::to_underlying_type(id)];
    }

    /**
     * @brief Reads value from given ID if it holds given type.
     * @tparam T Value type.
     * @param id Telemetry ID.
     * @return Optional with value on success.
     */
    template<typename T>
    satext::expected<T, std::monostate> read(telemetry_id id) {
        std::lock_guard lck(mtx_);
        if (auto* p = std::get_if<T>(&state_[satext::to_underlying_type(id)])) {
            return *p;
        } else {
            return satext::unexpected{std::monostate{}};
        }
    }

    void attach(telemetry_observer& observer) {
        std::lock_guard lck(mtx_);
        observers_.push_back(&observer);
    }

    void detach(telemetry_observer& observer) {
        std::lock_guard lck(mtx_);
        observers_.erase(std::ranges::find(observers_, &observer));
    }

private:
    std::span<telemetry_value> state_;
    etl::vector_ext<telemetry_observer*> observers_;
    satos::recursive_mutex mtx_{};
};

/** @} */

} // namespace telemetry
