#pragma once
#include <cstddef>
#include <span>
#include "telemetry/enums.h"
#include "telemetry/types.h"

namespace telemetry {

/**
 * @ingroup telemetry
 * @{
 */

/**
 * @brief Encodes single telemetry value to given buffer.
 * @param id Telemetry ID.
 * @param value Telemetry value.
 * @param buffer Buffer where encoded value will be written.
 * @return Size of encoded value.
 */
std::size_t encode(telemetry_id id, telemetry::telemetry_value value, std::span<std::byte> buffer);

/** @} */
} // namespace telemetry
