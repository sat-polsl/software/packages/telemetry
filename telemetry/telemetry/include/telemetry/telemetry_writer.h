#pragma once
#include <span>
#include "telemetry/telemetry_serializer.h"
#include "telemetry/telemetry_state_concept.h"

namespace telemetry {

/**
 * @ingroup telemetry
 * @{
 */

/**
 * @brief Class providing easy interface to serialize data from telemetry state to buffer of bytes.
 * @tparam State Serializable Telemetry State.
 */
template<telemetry::telemetry_state_concept State>
class telemetry_writer {
public:
    /**
     * @brief Constructor.
     * @param state Reference to telemetry state.
     * @param buffer Buffer of bytes.
     */
    telemetry_writer(State& state, std::span<std::byte> buffer) :
        serializer_{state},
        buffer_{buffer} {}

    /**
     * @brief Serializes values at given indexes from telemetry state to buffer of bytes.
     * @tparam Indexes Indexes type.
     * @param indexes Telemetry state indexes.
     */
    template<typename... Id>
    std::size_t write(Id... ids) {
        return ((single_write(ids)) + ...);
    }

private:
    std::size_t single_write(telemetry_id id) {
        auto size = serializer_.serialize(id, buffer_);
        buffer_ = buffer_.subspan(size);
        return size;
    }

    telemetry::telemetry_serializer<State> serializer_;
    std::span<std::byte> buffer_;
};

/** @} */

} // namespace telemetry
