#pragma once
#include <array>
#include <cstdint>
#include <variant>
#include "satext/inplace_function.h"
#include "telemetry/enums.h"
#include "telemetry/fwd.h"

namespace telemetry {

/**
 * @ingroup telemetry
 * @{
 */

/**
 * @brief Type holding telemetry value
 */
using telemetry_value = std::variant<std::monostate,
                                     std::uint8_t,
                                     std::uint16_t,
                                     std::uint32_t,
                                     std::int8_t,
                                     std::int16_t,
                                     std::int32_t,
                                     bool,
                                     float,
                                     char,
                                     std::byte>;

/**
 * @brief Type holding telemetry value buffer.
 * @tparam N Size of the buffer.
 */
template<std::size_t N>
using telemetry_value_buffer = std::array<telemetry_value, N>;

template<std::size_t N>
using telemetry_observer_buffer = std::array<telemetry_observer*, N>;

struct key_value {
    std::uint16_t id;
    telemetry_value value;
};

/** @} */

} // namespace telemetry
