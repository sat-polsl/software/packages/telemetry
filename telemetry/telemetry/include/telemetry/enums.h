#pragma once
#include <cstdint>

namespace telemetry {

/**
 * @ingroup telemetry
 * @{
 */

enum class telemetry_id : std::uint16_t {};

/**
 * @brief Telemetry type enumeration.
 */
enum class type_id : std::uint8_t {
    none = 0,
    u8 = 1,
    u16 = 2,
    u32 = 3,
    i8 = 4,
    i16 = 5,
    i32 = 6,
    boolean = 7,
    f32 = 8,
    character = 9,
    byte = 10
};

/** @} */
} // namespace telemetry
