#pragma once
#include <cstdint>
#include <span>
#include "satext/struct.h"
#include "satext/type_traits.h"
#include "telemetry/encode.h"
#include "telemetry/get_type_id.h"
#include "telemetry/telemetry_serializer.h"
#include "telemetry/telemetry_state_concept.h"

namespace telemetry {

/**
 * @ingroup telemetry
 * @{
 */

/**
 * @brief Encodes key-values from Telemetry State to bytes buffer.
 * @tparam State Telemetry State type.
 */
template<telemetry::telemetry_state_concept State>
class telemetry_encoder {
public:
    /**
     * @brief Constructor.
     * @param state Reference to Telemetry State.
     * @param buffer Buffer to serialize values.
     */
    telemetry_encoder(State& state, std::span<std::byte> buffer) : state_{state}, buffer_{buffer} {}

    /**
     * @brief Encodes single telemetry value to inner buffer.
     * @param id Telemetry id.
     * @return Size of encoded value.
     */
    std::size_t encode(telemetry::telemetry_id id) {
        auto size = ::telemetry::encode(id, state_.read(id), buffer_);
        buffer_ = buffer_.subspan(size);
        return size;
    }

    /**
     * @brief Checks whether inner buffer is empty.
     * @return True on empty, otherwise false;
     */
    [[nodiscard]] bool is_empty() const { return buffer_.empty(); }

private:
    State& state_;
    std::span<std::byte> buffer_;
};

/** @} */

} // namespace telemetry
