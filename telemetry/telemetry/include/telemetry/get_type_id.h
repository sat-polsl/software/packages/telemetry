#pragma once
#include "satext/overload.h"
#include "telemetry/enums.h"
#include "telemetry/types.h"

namespace telemetry {

/**
 * @ingroup telemetry
 * @{
 */

/**
 * @brief Returns type id for given telemetry value.
 * @param value Telemetry value.
 * @return Type id.
 */
type_id get_type_id(telemetry_value value);

/** @} */
} // namespace telemetry
