#pragma once
#include <cstdint>
#include <span>
#include "satext/expected.h"
#include "telemetry/types.h"

namespace telemetry {

/**
 * @ingroup telemetry
 * @{
 */

/**
 * @brief Decodes key-value serialized telemetry.
 */
class telemetry_decoder {
public:
    /**
     * @brief Constructor.
     * @param buffer Buffer with serialized telemetry.
     */
    explicit telemetry_decoder(std::span<const std::byte> buffer);

    /**
     * @brief Decodes single key-value serialized telemetry.
     * @return Key-value telemetry on success, monostate otherwise.
     */
    satext::expected<key_value, std::monostate> decode();

    /**
     * @brief Checks whether inner buffer is empty.
     * @return True on empty, otherwise false;
     */
    [[nodiscard]] bool is_empty() const;

private:
    std::span<const std::byte> buffer_;

    static constexpr auto index_shift = 4u;
    static constexpr auto type_mask = 0x0f;
    static constexpr auto key_size = sizeof(std::uint16_t);
};

/** @} */

} // namespace telemetry
