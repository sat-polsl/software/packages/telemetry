#pragma once
#include <variant>
#include "etl/vector.h"
#include "satext/overload.h"
#include "satos/chrono.h"
#include "satos/clock.h"
#include "satos/thread.h"
#include "telemetry/sampler_concept.h"
#include "telemetry/telemetry_state.h"

namespace telemetry {

/**
 * @ingroup telemetry
 * @{
 */

template<std::size_t StackSize, telemetry_state_concept State, sampler_concept... Samplers>
class telemetry_service;
template<std::size_t StackSize, telemetry_state_concept State, sampler_concept... Samplers>
using telemetry_service_base = satos::thread<telemetry_service<StackSize, State, Samplers...>,
                                             satos::thread_priority::normal_0,
                                             StackSize,
                                             "tlm">;

/**
 * @brief Service holding telemetry sources and calling them in proper order and timing.
 * @tparam StackSize Thread stack size.
 * @tparam State Telemetry state type.
 * @tparam Samplers Telemetry samplers.
 */
template<std::size_t StackSize, telemetry_state_concept State, sampler_concept... Samplers>
class telemetry_service : public telemetry_service_base<StackSize, State, Samplers...> {
    friend telemetry_service_base<StackSize, State, Samplers...>;

    using sampler_type = std::variant<std::monostate, std::reference_wrapper<Samplers>...>;

    struct sampler_data {
        sampler_type sampler{};
        satos::clock::time_point next{};
        satos::clock::duration interval{};

        auto operator<=>(const sampler_data& rhs) const { return this->next <=> rhs.next; };
    };

public:
    /**
     * @brief Constructor.
     * @param s Telemetry state reference.
     */
    explicit telemetry_service(State& s) : state_{s} {}

    /**
     * @brief Adds sampler to telemetry service with given interval.
     * @param sampler Sampler to add.
     * @param interval Sampler interval.
     */
    void add_sampler(sampler_concept auto& sampler, satos::clock::duration interval) {
        if (samplers_.full()) {
            return;
        }

        samplers_.emplace_back(sampler, satos::clock::next_time_point(interval), interval);
        etl::insertion_sort(samplers_.begin(), samplers_.end());
    }

private:
    void thread_function() {
        for (;;) {
            sampler_data& next_sampler = samplers_.front();

            if (satos::clock::now() < next_sampler.next) {
                satos::this_thread::sleep_until(next_sampler.next);
            }

            std::visit(
                satext::overload([this](auto& sampler) { sampler(state_); }, [](std::monostate) {}),
                next_sampler.sampler);

            next_sampler.next = satos::clock::next_time_point(next_sampler.interval);

            etl::insertion_sort(samplers_.begin(), samplers_.end());
        }
    }

    State& state_;

    etl::vector<sampler_data, sizeof...(Samplers)> samplers_;
};

/** @} */

} // namespace telemetry
