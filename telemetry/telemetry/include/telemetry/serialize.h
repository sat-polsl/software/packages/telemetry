#pragma once
#include <cstddef>
#include <span>
#include "telemetry/types.h"

namespace telemetry {

/**
 * @ingroup telemetry
 * @{
 */

/**
 * @brief Serializes given telemetry value to given buffer.
 * @param value Telemetry value.
 * @param buffer Buffer where serialized value will be written.
 * @return Size of written bytes.
 */
std::size_t serialize(telemetry_value value, std::span<std::byte> buffer);

/** @} */
} // namespace telemetry
