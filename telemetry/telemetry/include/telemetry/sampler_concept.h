#pragma once
#include <concepts>
#include <telemetry/telemetry_state_concept.h>

namespace telemetry {
// clang-format off
template<typename T>
concept sampler_concept = requires (T sampler, typename T::state state) {
    sampler(state);
} && telemetry_state_writable<typename T::state>;
// clang-format on
} // namespace telemetry
