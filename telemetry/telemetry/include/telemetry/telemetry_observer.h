#pragma once
#include "satext/inplace_function.h"
#include "telemetry/enums.h"
#include "telemetry/fwd.h"

namespace telemetry {

class telemetry_observer {
public:
    using on_update = satext::inplace_function<void(telemetry_id, telemetry_state&)>;

    explicit telemetry_observer(on_update update) : update_{std::move(update)} {}

    void update(telemetry_id id, telemetry_state& state) const { update_(id, state); }

private:
    on_update update_;
};

} // namespace telemetry
