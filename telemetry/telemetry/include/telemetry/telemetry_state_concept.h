#pragma once
#include <concepts>
#include <cstdint>
#include <mutex>
#include <optional>
#include <span>
#include <variant>
#include "satext/overload.h"
#include "satext/struct.h"
#include "satos/concepts/mutex.h"
#include "telemetry/enums.h"

namespace telemetry {

// clang-format off
template<typename T>
concept telemetry_state_writable = requires(T t, telemetry_id id, std::uint8_t u8,
                                            std::uint16_t u16, std::uint32_t u32, std::int8_t i8,
                                            std::int16_t i16, std::int32_t i32, bool boolean,
                                            float f, char c, std::byte b, std::monostate m) {
    t.write(id, u8);
    t.write(id, u16);
    t.write(id, u32);
    t.write(id, i8);
    t.write(id, i16);
    t.write(id, i32);
    t.write(id, boolean);
    t.write(id, f);
    t.write(id, c);
    t.write(id, b);
    t.write(id, m);
};

template<typename T>
concept telemetry_state_readable = requires(T t, telemetry_id id, std::uint32_t value) {
    t.read(id);
};

template<typename T>
concept telemetry_state_serializable = requires(T t, telemetry_id id, std::span<std::byte> buffer) {
    {t.serialize(id, buffer)} -> std::same_as<std::size_t>;
};

template<typename T>
concept telemetry_state_concept = telemetry_state_writable<T> && telemetry_state_readable<T>;
// clang-format on

} // namespace telemetry
